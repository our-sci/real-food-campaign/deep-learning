#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import keras
from keras.models import Sequential
from keras.layers import Dense

from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


# reflection = ("assay_1.data.median_365", "assay_1.data.median_385", "assay_1.data.median_450", "assay_1.data.median_500", "assay_1.data.median_530",
#              "assay_1.data.median_587", "assay_1.data.median_632", "assay_1.data.median_850", "assay_1.data.median_880", "assay_1.data.median_940")


scan_type = "produce.rawscan1"

input_keys = (
    "{}.data.median_365".format(scan_type),
    "{}.data.median_385".format(scan_type),
    "{}.data.median_450".format(scan_type),
    "{}.data.median_500".format(scan_type),
    "{}.data.median_530".format(scan_type),
    "{}.data.median_587".format(scan_type),
    "{}.data.median_632".format(scan_type),
    "{}.data.median_850".format(scan_type),
    "{}.data.median_880".format(scan_type),
    "{}.data.median_940".format(scan_type)
)

csv_file = "data_rfc_2019.csv"
variety_column = "sample_type"
variety = "cherry_tomato"
target_column = "produce.get_chemisty.data.polyphenols_value"

threshold = 125


def main():
    # df = np.genfromtxt("./data.csv", delimiter=",")
    df = pd.read_csv(csv_file, sep=",")
    # df = df[df["qc_id"].notnull()]
    df = df[df[target_column].notnull()]
    df = df[df[variety_column] == variety]

    for col in input_keys:
        df = df[df[col].notnull()]

    first = df.columns.get_loc(input_keys[0])
    target = df.columns.get_loc(target_column)

    X = df.iloc[:, first:(first+len(input_keys))].values
    y_float = df.iloc[:, target:(target+1)].values

    print("float vals")
    print(y_float)

    y = [0] * len(y_float)

    h = [0] * len(y_float)
    count = 0
    for idx, v in enumerate(y_float):
        #y[idx] = [0 if ("organic" in v) else 1]
        #h[idx] = 0 if ("organic" in v) else 1
        y[idx] = [0 if v < threshold else 1]
        count += y[idx][0]

    #plt.title('Number of Samples {:.2f}'.format(len(y_float)))
    #plt.hist(h, bins=2)
    # plt.show()

    plt.title('Number of Samples {} median {}'.format(
        len(y_float), np.median(y_float)))
    plt.suptitle("{} Samples match criteria (top {:.1f}%)".format(
        count, count / len(y_float) * 100))
    plt.hist(y_float, bins=25)
    plt.show()

    print(y)
    sc = StandardScaler()
    X = sc.fit_transform(X)
    print('Normalized data:')
    print(X)

    # One hot encode
    ohe = OneHotEncoder()
    y = ohe.fit_transform(y).toarray()
    print('One hot encoded array:')
    print(y)

    # Train test split of model
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=0)
    # df = df[df["assay_1.data.antioxidants"].notnull()]

    model = Sequential()

    model.add(Dense(7, input_dim=len(input_keys), activation='sigmoid'))
    # model.add(Dense(6, activation='relu'))
    model.add(Dense(5, activation='relu'))
    # model.add(Dense(3, activation='relu'))
    model.add(Dense(2, activation='softmax'))

    model.summary()

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])

    history = model.fit(X_train, y_train, epochs=1000, batch_size=64, validation_data=(
        X_test, y_test))
    y_pred = model.predict(X_test)

    # Converting predictions to label
    pred = list()
    for i in range(len(y_pred)):
        pred.append(np.argmax(y_pred[i]))

    test = list()
    for i in range(len(y_test)):
        test.append(np.argmax(y_test[i]))

    a = accuracy_score(pred, test)

    print('Accuracy is:', a*100)
    print(history.history.keys())

    fig, plots = plt.subplots(2)

    fig.set_figwidth(16)
    fig.set_figheight(9)
    fig.suptitle("{} > {}".format(target_column, threshold))

    plots[0].plot(history.history['accuracy'])
    plots[0].plot(history.history['val_accuracy'])
    plots[0].set(title='Model Accuracy {}'.format(
        variety), xlabel='Epoch', ylabel='Accuracy')
    plots[0].set_ylim(bottom=0.5, top=1.0)
    plots[0].legend(['Train', 'Test'], loc='upper left')

    plots[1].plot(history.history['loss'])
    plots[1].plot(history.history['val_loss'])
    plots[1].set(title='Model loss {}'.format(
        variety), xlabel='Epoch', ylabel='Loss')
    plots[1].set_ylim(bottom=0, top=1)
    plots[1].legend(['Train', 'Test'], loc='upper left')

    plt.show()


if __name__ == "__main__":
    main()
